//
//  TKNAppDelegate.h
//  MoneyManagement
//
//  Created by Truong Khanh Nguyen on 6/7/14.
//  Copyright (c) 2014 Truong Khanh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
