//
//  main.m
//  MoneyManagement
//
//  Created by Truong Khanh Nguyen on 6/7/14.
//  Copyright (c) 2014 Truong Khanh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TKNAppDelegate class]));
    }
}
